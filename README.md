SparrowHack
===================

Backend-as-a-Service for online games. (Working in progress)

This project follows the Nomad architecture to build a highly distributed backend as a service for running online MOBA games (i.e. League of Legends, Clash Royale). In this type of games, teams fight each other in a limited area for a limited period of time. This type of problem is highly scalable since independent client servers can be added to independently handle different battles.

----------

### Architecture

Following the nomad architecture, we define two types of nodes: servers and clients. Whenever a client wants to join the game, it registers with the server with an authorization method (social id, user-pass...). Then, when it wants to join a new 'battle' it makes a requests to the servers. Then, the server gives him the direction of one of the clients and the id of the new 'battle'. Next, the user joins to the client with websockets with the specific id. On the background, the servers uses rpc to communicate with the client and registers new battles if necessary.

#### Server

Handle the requests of the users for authorization, authentication and 'battle' joining. This is done through an API Rest.

#### Client

It connects with RPC to the servers. The users connects to the clients through websocket to start the battle. The client implements the logic of the battle.
