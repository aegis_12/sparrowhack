package server

import "log"

type endpoints struct {
	Game *Game
	User *User
}

type Server struct {
	logger log.Logger
	config *Config

	endpoints endpoints
}

func NewServer(config *Config, logger log.Logger) (*Server, error) {
	return &Server{
		logger: logger,
		config: config,
	}, nil
}

func (s *Server) registerEndpoints() error {
	s.endpoints.Game = &Game{s}
	s.endpoints.User = &User{s}

	return nil
}
