package server

type Game struct {
	srv *Server
}

// gameCreateRequest, gameCreateResponse
func (g *Game) Create() {

}

// GameSpecificRequest, SingleGameResponse
func (g *Game) GetGame() {

}

// GameListRequest, GameListResponse
func (g *Game) List() {

}
