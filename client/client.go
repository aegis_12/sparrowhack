package client

import "log"

type Client struct {
	logger log.Logger
	config *Config
}

func NewClient(config *Config, logger log.Logger) (*Client, error) {
	return &Client{
		logger: logger,
		config: config,
	}, nil
}
